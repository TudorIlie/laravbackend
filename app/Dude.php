<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dude extends Model
{
    public $timestamps = false;
    protected $fillable=['password','email','phone','token_email','token_sms','token_user']; 



    public function profile(){
        return $this->hasOne('App\Profile');
    }
}
