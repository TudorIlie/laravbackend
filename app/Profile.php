<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable=['firstName','lastName','dude_id'];


    public function adress(){
        return $this->hasMany('App\Adress');
    }
}
