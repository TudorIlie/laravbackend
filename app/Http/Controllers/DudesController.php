<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dude;
use App\Profile;
use App\Adress;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class DudesController extends Controller
{
    /* public function Register(Request $request){
        if(!Dude::where('email', '=', $request->email)->exists()){

            Dude::insert($request->all());
            return "Record added to DB";
        }
        else{
            
            return "Record not added to DB";
        }
            
            
    }
 */

public function Register(Request $request){
    
    $token_sms=Str::random(60);
    $token_email=Str::random(60);
    $token_user=Str::random(60);

    while(Dude::where('token_sms', '=', $token_sms)->exists()){
        $token_sms=Str::random(60);
    }
    while(Dude::where('token_email', '=', $token_email)->exists()){
        $token_email=Str::random(60);
    }
    while(Dude::where('token_sms', '=', $token_user)->exists()){
        $token_user=Str::random(60);
    }
    
    $hashed=Hash::make($request->password);

    if(Dude::where('email', '=', $request->email)->exists()){
        return "Email allready in use";
    }
    else if(Dude::where('phone', '=', $request->phone)->exists()){
        return "Phone number allready in use";
    }
    else{
        $dude=new Dude([
            'password'=>$hashed,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'token_sms'=>$token_sms,
            'token_email'=>$token_email,
            'token_user'=>$token_user,
        ]);
    
        $dude->save();
        $profile = new Profile;
        $profile->firstName=$request->firstName;
        $profile->lastName=$request->lastName;

        $dude->profile()->save($profile);
    }
    $ToReturn=array(
                $dude->profile->firstName,
                $dude->profile->lastName,
                $dude->phone,
                $dude->email,
                $dude->password
    );
    

    // Dude::create([
    //     'password'=>$request->password,
    //     'email'=>$request->email
    // ]);

    return $ToReturn;
}

    public function Login(Request $request){

        //$users = Dude::table('users')->get();
        
        $email=$request->email;
        $pass=$request->password;
        
        if(Dude::where('email', '=', $email)->exists()){
            $dude=Dude::where('email', '=', $email)->first();
            if(Hash::check($pass, Dude::where('email', '=', $email)->first()->password)){    
                $dude->first_login=true;
                $dude->save();
                return $dude->profile;
            }
            else
                return "Incorect password or email";
        }
        else
            return "No records found";
    }

    public function Edit(Request $request){

        // $email=$request->email;
        // $pass=$request->password;
        // $phone=$request->phone;


        // $dude= Dude::where('id','=', 1)->first();
        // if($dude){
        //     $dude->email=$email;
        //     $dude->password=$pass;
        //     $dude->phone=$phone;

        //     $dude->save();
        // }
        return $request;


    }
    
    public function Adress(Request $request){
        $adress = new Adress;
        $adress->street=$request->street;
        $adress->city=$request->city;
        $adress->streetNumber=$request->streetNumber;
        $email=$request->email;
        $dude=Dude::where('email', '=', $email)->first();
        if($dude){
            $dude->profile->adress()->save($adress);
        }
        return $adress;
    }

    public function EditAUX(Request $request){
        $dude=Dude::where('email', '=', $request->email)->first();
        if($dude){
            if($request->firstName){
                $dude->profile->firstName=$request->firstName;
            }
            if($request->lastName){
                $dude->profile->lastName=$request->lastName;
            }
            if($request->birthDate){
                $dude->profile->birthDate=$request->birthDate;
            }
            $dude->profile->save();
            
            return $dude->profile;
        }
    }

    public function EditAUXadress(Request $request){
        $dude=Dude::where('email', '=', $request->email)->first();
        $adressID=$request->adrressId;
        if($dude){
            $adress=Adress::where([['profile_id', '=', $dude->id], ['id', '=', $adressID]])->first();
            if($adress)
            {
                if($request->street){
                    $adress->street=$request->street;
                }
                if($request->city){
                    $adress->city=$request->city;
                }
                if($request->streetNumber){
                    $adress->streetNumber=$request->streetNumber;
                }
                $adress->save();
                return $adress;
            }
            else{
                $adress=new Adress;
                $adress->street=$request->street;
                $adress->city=$request->city;
                $adress->streetNumber=$request->streetNumber;
                $dude->profile->adress()->save($adress);
                return $adress;
            }
            
        }
    }
    
}
