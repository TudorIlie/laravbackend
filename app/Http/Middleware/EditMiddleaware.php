<?php

namespace App\Http\Middleware;

use Closure;

class EditMiddleaware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(&$request, Closure $next, $userId, $userToken)
    {
        
        if($userId && $userToken){
            return $next($request);
        }
        else{
            return redirect('home');
        }
    }
}
