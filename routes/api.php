<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/register', 'DudesController@Register');
Route::get('/login', 'DudesController@Login');
//Route::get('/edit', 'DudesController@Edit')->middleware('editMiddleware:ceva1,ceva2');
Route::get('/adress', 'DudesController@Adress');
Route::get('/edit', 'DudesController@EditAUX');
Route::get('/editAdress', 'DudesController@EditAUXadress');
