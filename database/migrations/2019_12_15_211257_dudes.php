<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Dudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dudes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('password');
            $table->string('email')->unique();
            $table->string('phone', 10)->nullable()->unique();
            $table->string('token_email');
            $table->string('token_sms');
            $table->string('token_user');
            $table->boolean('first_login')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dudes');
    }
}
